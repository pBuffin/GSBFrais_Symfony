Symfony
=======

Refonte de mon projet de BTS, GSBFrais sous Symfony.

## Deploiment

### utilitaire nécessaire à l'installation de l'application 
Un serveur HTTP comme [Xampp](https://www.apachefriends.org/fr/index.html),<br>
Le SGBD [mysql](https://www.mysql.com/fr/downloads/), (inutile si Xampp est utilisé)<br>
Le Gestionnaire de paquet [Composer](https://getcomposer.org/),<br>
Un émulateur de console Comme [Commander](http://cmder.net/)

Apres avoir cloné l'application à la racine du serveur HTTP, ouvrez un fenêtre console dans le répertoire GSBFrais du serveur.<br>Pour installer les dépendances de Vendor taper la commande suivante  : ` $ composer install`<br>
Puis crée la base de donnée en tapant `$ php bin/console doctrine:schema:update --force`
Remplisser la la base de donnée en tapant `$ php bin/console doctrine:fixtures:load`

## Connexion
Pour les viviteurs et les Comptables
Pour vous connecter Login = cbedos et mot de passe = secret.