<?php

namespace GSBFrais\ConnexionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('GSBFraisConnexionBundle:Default:index.html.twig');
    }
}
