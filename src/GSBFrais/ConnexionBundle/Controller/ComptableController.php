<?php

namespace GSBFrais\ConnexionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use GSBFrais\ConnexionBundle\Entity\Visiteur;
use Symfony\Component\HttpFoundation\Request;
use GSBFrais\ConnexionBundle\Entity\fichefrais;
use GSBFrais\ConnexionBundle\Entity\lignefraishorsforfait;
use GSBFrais\ConnexionBundle\Entity\lignefraisforfait;
use GSBFrais\ConnexionBundle\Entity\fraisforfait;

class ComptableController extends Controller {

    public function accueilComptableAction(SessionInterface $session) {
        $em = $this->getDoctrine()->getEntityManager();
        $Comptable = $em->getRepository('GSBFraisConnexionBundle:Comptable')->findOneBy(array('id' => $session->get('id')));

        return $this->render('Comptable.html.twig', array('comptable' => $Comptable));
    }

    public function compteComptableAction(SessionInterface $session) {

        $em = $this->getDoctrine()->getEntityManager();
        $Comptable = $em->getRepository('GSBFraisConnexionBundle:Comptable')->findOneBy(array('id' => $session->get('id')));

        return $this->render('CompteComptable.html.twig', array('comptable' => $Comptable));
    }

    public function consulterFicheAction(SessionInterface $session) {

        $em = $this->getDoctrine()->getEntityManager();
        $Comptable = $em->getRepository('GSBFraisConnexionBundle:Comptable')->findOneBy(array('id' => $session->get('id')));
        $repositoryv = $this->getDoctrine()->getRepository(Visiteur::class);
        $ligneVisiteur = $repositoryv->findAll();
        $em->flush();
        return $this->render('ValidationFiche.html.twig', array('comptable' => $Comptable, 'ligneVisiteur' => $ligneVisiteur, 'idVisiteur' => '', 'listeMois' => null, 'ligneForfait' => null, 'ligneHorsForfait' => null, 'nbJustificatifs' => null));
    }

    public function suivreFicheAction(SessionInterface $session) {

        $em = $this->getDoctrine()->getEntityManager();
        $Comptable = $em->getRepository('GSBFraisConnexionBundle:Comptable')->findOneBy(array('id' => $session->get('id')));

        $qb = $em->createQueryBuilder('p');
        $qb->select('v.nom', 'v.prenom','ff.id', 'ff.mois', 'ff.nbjustificatifs', 'ff.montantValide', 'ff.dateModif', 'ff.idEtat')
                ->from('GSBFrais\ConnexionBundle\Entity\Visiteur', 'v')
                ->innerjoin('GSBFrais\ConnexionBundle\Entity\fichefrais', 'ff', 'WITH', 'v.id = ff.idVisiteur');

        $query = $qb->getQuery();
        $fiches = $query->getResult();

        foreach ($fiches as $key => $value) {
            $mois = $this->dateLettre($value['mois']);
            $fiches[$key]['mois'] = $mois;
            ;
        }
       
        return $this->render('SuivieFiche.html.twig', array('comptable' => $Comptable, 'listeFiche' => $fiches));
    }

    public function deconnexionComptableAction(SessionInterface $session) {
        $session->clear();
        return $this->render('GSBFraisConnexionBundle:Default:index.html.twig');
    }

    public function rechercheFicheAction(SessionInterface $session) {
        $em = $this->getDoctrine()->getEntityManager();
        $Comptable = $em->getRepository('GSBFraisConnexionBundle:Comptable')->findOneBy(array('id' => $session->get('id')));
        $repositoryv = $this->getDoctrine()->getRepository(Visiteur::class);

        $ligneVisiteur = $repositoryv->findAll();

        $request = Request::createFromGlobals();
        $id = $request->request->get('visiteur');

        $repositoryff = $this->getDoctrine()->getRepository(fichefrais::class);
        $query = $repositoryff->createQueryBuilder('f')
                ->select('f.mois')
                ->where('f.idVisiteur = :idVisiteur')
                ->setParameter('idVisiteur', $id)
                ->getQuery();

        $lignes = $query->getResult();
        $vli = [];
        foreach ($lignes as $ligne) {
            $dateLettre = $this->dateLettre($ligne['mois']);
            $vli[$ligne['mois']] = $dateLettre;
        }

        $em->flush();
        return $this->render('ValidationFiche.html.twig', array('comptable' => $Comptable, 'ligneVisiteur' => $ligneVisiteur, 'idVisiteur' => $id, 'listeMois' => $vli, 'ligneForfait' => null, 'ligneHorsForfait' => null, 'nbJustificatifs' => null, 'date' => ''));
    }

    public function dateLettre($date) {
        $tabmois = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];
        $m = (int) substr($date, 0, 2);
        $mois = $tabmois[$m - 1];
        $annee = substr($date, 2);
        $dateLettre = $mois . " " . $annee;

        return $dateLettre;
    }

    public function afficheFicheAction(SessionInterface $session) {
        $repositorylff = $this->getDoctrine()->getRepository(lignefraisforfait::class);
        $repositorylfhf = $this->getDoctrine()->getRepository(lignefraishorsforfait::class);
        $repositoryff = $this->getDoctrine()->getRepository(fichefrais::class);
        $em = $this->getDoctrine()->getEntityManager();
        $Comptable = $em->getRepository('GSBFraisConnexionBundle:Comptable')->findOneBy(array('id' => $session->get('id')));
        $repositoryv = $this->getDoctrine()->getRepository(Visiteur::class);
        $ligneVisiteur = $repositoryv->findAll();

        $request = Request::createFromGlobals();
        $id = $request->request->get('visiteur');
        $mois = $request->request->get('mois');
        $visiteur = $em->getRepository('GSBFraisConnexionBundle:Visiteur')->findOneBy(array('id' => $id));


        $query = $repositoryff->createQueryBuilder('f')
                ->select('f.mois')
                ->where('f.idVisiteur = :idVisiteur')
                ->setParameter('idVisiteur', $id)
                ->getQuery();

        $lignes = $query->getResult();
        $vli = [];
        foreach ($lignes as $ligne) {
            $dateLettre = $this->dateLettre($ligne['mois']);
            $vli[$ligne['mois']] = $dateLettre;
        }

        $query = $repositorylff->createQueryBuilder('f')
                ->select('f.idFraisForfait', 'f.quantite')
                ->where('f.idVisiteur = :idVisiteur')
                ->andWhere('f.mois = :mois')
                ->setParameter('idVisiteur', $id)
                ->setParameter('mois', $mois)
                ->getQuery();
        $result = $query->getScalarResult();
        $ligneforfait = array_column($result, 'quantite', 'idFraisForfait');
        $lignehorsforfait = $repositorylfhf->findBy(array('idVisiteur' => $id, 'mois' => $mois));
        $fichefrais = $repositoryff->findOneBy(array('idVisiteur' => $id, 'mois' => $mois));
        $dateLettre = $this->dateLettre($mois);
        $em->flush();

        $nbJustificatifs = $fichefrais->getNbjustificatifs();

        return $this->render('ValidationFiche.html.twig', array('comptable' => $Comptable, 'ligneVisiteur' => $ligneVisiteur, 'idVisiteur' => $id, 'listeMois' => $vli, 'ligneForfait' => $ligneforfait, 'ligneHorsForfait' => $lignehorsforfait, 'nbJustificatifs' => $nbJustificatifs, 'date' => $mois, 'leVisiteur' => $visiteur, 'dateLettre' => $dateLettre, 'fiche' => $fichefrais));
    }

    public function actualiserForfaitAction(SessionInterface $session) {
        $em = $this->getDoctrine()->getEntityManager();
        $request = Request::createFromGlobals();
        $repositorylff = $this->getDoctrine()->getRepository(lignefraisforfait::class);

        $etape = $request->request->get('etp');
        $kilo = $request->request->get('km');
        $nuit = $request->request->get('nui');
        $resto = $request->request->get('rep');
        $id = $request->request->get('visiteur');
        $mois = $request->request->get('mois');

        $ligneetp = $repositorylff->findOneBy(array('idVisiteur' => $id, 'mois' => $mois, 'idFraisForfait' => 'ETP'));
        $lignenui = $repositorylff->findOneBy(array('idVisiteur' => $id, 'mois' => $mois, 'idFraisForfait' => 'NUI'));
        $lignekm = $repositorylff->findOneBy(array('idVisiteur' => $id, 'mois' => $mois, 'idFraisForfait' => 'KM'));
        $ligneresto = $repositorylff->findOneBy(array('idVisiteur' => $id, 'mois' => $mois, 'idFraisForfait' => 'REP'));

        $ligneetp->setQuantite($etape);
        $lignenui->setQuantite($nuit);
        $lignekm->setQuantite($kilo);
        $ligneresto->setQuantite($resto);

        $em->persist($ligneetp);
        $em->persist($lignenui);
        $em->persist($lignekm);
        $em->persist($ligneresto);

        $em->flush();

        $view = $this->afficheFicheAction($session);

        return $view;
    }

    public function reinitialiserForfaitAction(SessionInterface $session) {
        $view = $this->afficheFicheAction($session);
        return $view;
    }

    public function ValiderFicheAction(SessionInterface $session) {
        $em = $this->getDoctrine()->getEntityManager();
        $request = Request::createFromGlobals();
        $repositoryff = $this->getDoctrine()->getRepository(fichefrais::class);
        $repositorylff = $this->getDoctrine()->getRepository(lignefraisforfait::class);
        $repositorylfhf = $this->getDoctrine()->getRepository(lignefraishorsforfait::class);
        $repositoryetat = $this->getDoctrine()->getRepository(fraisforfait::class);

        $id = $request->request->get('visiteur');
        $mois = $request->request->get('mois');

        $fraisforfait = $repositorylff->findBy(array('idVisiteur' => $id, 'mois' => $mois));
        $fraishorsforfait = $repositorylfhf->findBy(array('idVisiteur' => $id, 'mois' => $mois));
        $frais = $repositoryetat->findAll();

        $base = [];
        foreach ($frais as $key => $value) {

            $base[$value->getSigle()] = $value->getMontant();
        }
        $montantFF = 0;
        foreach ($fraisforfait as $key => $value) {
            $montantFF = $montantFF + $value->getQuantite() * $base[$value->getIdFraisForfait()];
        }

        $montantHF = 0;
        foreach ($fraishorsforfait as $key => $value) {
            if (!(substr($value->getLibelle(), 0, 8) == 'REFUSE :')) {
                $montantHF = $montantHF + $value->getMontant();
            }
        }

        $montantTotal = $montantFF + $montantHF;

        $fiche = $repositoryff->findOneBy(array('idVisiteur' => $id, 'mois' => $mois));
        $fiche->setMontantValide($montantTotal);
        $fiche->setIdEtat('VA');

        $em->persist($fiche);
        $em->flush();

        $view = $this->afficheFicheAction($session);
        return $view;
    }

    public function actualiserHorsForfaitAction($id, SessionInterface $session) {
        $em = $this->getDoctrine()->getEntityManager();
        $request = Request::createFromGlobals();
        $montant = $request->request->get($id);
        $repositorylfhf = $this->getDoctrine()->getRepository(lignefraishorsforfait::class);

        $ligne = $repositorylfhf->findOneBy(array('id' => $id));

        $ligne->setMontant($montant);
        $em->persist($ligne);
        $em->flush();

        $view = $this->afficheFicheAction($session);

        return $view;
    }

    public function supprimerHorsForfaitAction($id, SessionInterface $session) {
        $em = $this->getDoctrine()->getEntityManager();
        $repositorylfhf = $this->getDoctrine()->getRepository(lignefraishorsforfait::class);
        $ligne = $repositorylfhf->find($id);

        $ligne->setLibelle('REFUSE :' . $ligne->getLibelle());
        $em->persist($ligne);
        $em->flush();

        $view = $this->afficheFicheAction($session);

        return $view;
    }

    public function reporterHorsForfaitAction($id, SessionInterface $session) {
        $em = $this->getDoctrine()->getEntityManager();
        $repositorylfhf = $this->getDoctrine()->getRepository(lignefraishorsforfait::class);
        $ligne = $repositorylfhf->find($id);
        $date = $ligne->getMois();
        $idVisiteur = $ligne->getidVisiteur();


        $mois = (int) substr($date, 0, 2) + 1;
        $annee = substr($date, 2);
        if ($mois < 10) {
            $mois = "0" . $mois;
        } elseif ($mois == 12) {
            $mois = "01";
            $annee = (int) $annee + 1;
        }
        $date = $mois . $annee;

        $repositoryff = $this->getDoctrine()->getRepository(fichefrais::class);
        $fiche = $repositoryff->findOneBy(array('idVisiteur' => $idVisiteur, 'mois' => $date)
        );

        if (!$fiche) {
            $fiche = new fichefrais();
            $fiche->setIdVisiteur($idVisiteur)
                    ->setMois($date)
                    ->setMontantValide(0)
                    ->setNbjustificatifs(0)
                    ->setDateModif(new \DateTime("now"))
                    ->setIdEtat('CR');
            $em->persist($fiche);
        }

        $ligne->setMois($date);
        $em->persist($ligne);
        $em->flush();

        $view = $this->afficheFicheAction($session);

        return $view;
    }

    public function rembourserAction($id,SessionInterface $session) {
       
        $em = $this->getDoctrine()->getEntityManager();
        $repositoryff = $this->getDoctrine()->getRepository(fichefrais::class);
        
        $fiche = $repositoryff->find($id);
        $fiche->setIdEtat('RB');
        $em->persist($fiche);
        $em->flush();
        
        $view = $this->suivreFicheAction($session);

        return $view;
    }

}
