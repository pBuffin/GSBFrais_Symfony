<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace GSBFrais\ConnexionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

class ConnexionController extends Controller {

    public function ConnectAction() {
        $request = Request::createFromGlobals();
        $erreur = "";
        $login = $request->request->get('login');
        $pwd = $request->request->get('password');
        $role = $request->request->get('role');
//        vérfication du role qui se connecte
        $connected=false;
        switch ($role) {
//            connexion d'un visiteur'
            case 'visiteur':
                $em = $this->getDoctrine()->getEntityManager();
                $visiteur = $em->getRepository('GSBFraisConnexionBundle:Visiteur')->findOneBy(array('login'=>$login));
                
                if ($visiteur->getMdp() == $pwd) {
                    
                    $session = new Session();
                    if($session->isStarted()) {
                        $session->start();
                    }
                        $session->set('id', $visiteur->getId());
                        $session->set('nom', $visiteur->getNom());
                        $session->set('prenom', $visiteur->getPrenom());
                        
                        $connected = true;
                    
                }

                if ($connected) {
                    return $this->render('Visiteur.html.twig', array('visiteur' => $visiteur));
                } else {
                    $erreur = "Login ou Mot de passe incorrect";
                    //return $this->render('index.html.twig', compact('erreur'));
                }
                break;
//          connexion d'un comptable'
            case 'comptable':
                $em = $this->getDoctrine()->getEntityManager();
                $Comptable = $em->getRepository('GSBFraisConnexionBundle:Comptable')->findOneBy(array('login'=>$login));

                if ($Comptable->getMdp() == $pwd) {

                    $session = new Session();
                    if($session->isStarted()) {
                        $session->start();
                    }

                    $session->set('id', $Comptable->getId());
                    $session->set('nom', $Comptable->getNom());
                    $session->set('prenom', $Comptable->getPrenom());

                    $connected = true;
                }

                if ($connected) {
                    return $this->render('Comptable.html.twig', array('comptable' => $Comptable));
                } else {
                    $erreur = "Login ou Mot de passe incorrect";
                    //return $this->render('index.html.twig', compact('erreur'));
                }
                break;
        }

        return new response($erreur);
    }

}
