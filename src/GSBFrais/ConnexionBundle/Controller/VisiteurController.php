<?php

namespace GSBFrais\ConnexionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;
use GSBFrais\ConnexionBundle\Entity\lignefraisforfait;
use GSBFrais\ConnexionBundle\Entity\lignefraishorsforfait;
use GSBFrais\ConnexionBundle\Entity\fichefrais;

class VisiteurController extends Controller {

    public function AccueilAction(SessionInterface $session) {

        $em = $this->getDoctrine()->getEntityManager();
        $visiteur = $em->getRepository('GSBFraisConnexionBundle:Visiteur')->findOneBy(array('id' => $session->get('id')));

        return $this->render('Visiteur.html.twig', array('visiteur' => $visiteur));
    }

    public function monCompteAction(SessionInterface $session) {


        $em = $this->getDoctrine()->getEntityManager();
        $visiteur = $em->getRepository('GSBFraisConnexionBundle:Visiteur')->findOneBy(array('id' => $session->get('id')));

        return $this->render('CompteVisiteur.html.twig', array('visiteur' => $visiteur));
    }

    public function renseigneFicheAction(SessionInterface $session) {
        setlocale(LC_TIME, 'fr', 'fr_FR', 'fr_FR.ISO8859-1');
        $annee = strftime('%Y');
        $mois = strftime('%B');
        $titre = "Renseigner ma fiche de frais du mois de " . $mois . " " . $annee;
        $em = $this->getDoctrine()->getEntityManager();
        $visiteur = $em->getRepository('GSBFraisConnexionBundle:Visiteur')->findOneBy(array('id' => $session->get('id')));

        return $this->render('SaisieFicheFrais.html.twig', array('titre' => $titre, 'visiteur' => $visiteur));
    }

    public function fichefraisAction(SessionInterface $session) {
        $em = $this->getDoctrine()->getEntityManager();

        $visiteur = $em->getRepository('GSBFraisConnexionBundle:Visiteur')->findOneBy(array('id' => $session->get('id')));

        $repositoryff = $this->getDoctrine()->getRepository(fichefrais::class);
        $query = $repositoryff->createQueryBuilder('f')
                ->select('f.mois')
                ->where('f.idVisiteur = :idVisiteur')
                ->setParameter('idVisiteur', $session->get('id'))
                ->getQuery();

        $lignes = $query->getResult();
        $vli = [];
        foreach ($lignes as $ligne) {
            $dateLettre = $this->dateLettre($ligne['mois']);
            $vli[$ligne['mois']] = $dateLettre;
        }

        return $this->render('FicheFraisVisiteur.html.twig', array('visiteur' => $visiteur, 'lignes' => $vli, 'ligneForfait' => '', 'ligneHorsForfait' => '', 'nbJustificatifs' => ''));
    }

    public function deconnexionAction(SessionInterface $session) {

        $session->clear();
        return $this->render('GSBFraisConnexionBundle:Default:index.html.twig');
    }

    public function forfaitAction(SessionInterface $session) {
        $request = Request::createFromGlobals();
        setlocale(LC_TIME, 'fr', 'fr_FR', 'fr_FR.ISO8859-1');
        $annee = strftime('%Y');
        $mois = strftime('%B');
        $titre = "Renseigner ma fiche de frais du mois de " . $mois . " " . $annee;
        $em = $this->getDoctrine()->getEntityManager();
        $visiteur = $em->getRepository('GSBFraisConnexionBundle:Visiteur')->findOneBy(array('id' => $session->get('id')));
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        $repositoryff = $this->getDoctrine()->getRepository(fichefrais::class);
        $fiche = $repositoryff->findOneBy(
                array('idVisiteur' => $session->get('id'), 'mois' => strftime('%m') . $annee)
        );

        if (!$fiche) {
            $fiche = new fichefrais();
            $fiche->setIdVisiteur($session->get('id'))
                    ->setMois(strftime('%m') . $annee)
                    ->setMontantValide(0)
                    ->setNbjustificatifs(0)
                    ->setDateModif(new \DateTime("now"))
                    ->setIdEtat('CR');
            $em->persist($fiche);
        }

        $etape = $request->request->get('etape');
        $kilo = $request->request->get('kilo');
        $nuit = $request->request->get('nuit');
        $resto = $request->request->get('resto');

        $repositorylff = $this->getDoctrine()->getRepository(lignefraisforfait::class);

        if ($etape) {
            $etp = $repositorylff->findOneBy(
                    array('idVisiteur' => $session->get('id'), 'mois' => strftime('%m') . $annee, 'idFraisForfait' => 'ETP')
            );
            if (!$etp) {
                $etp = new lignefraisforfait();
                $etp->setIdVisiteur($session->get('id'))
                        ->setMois(strftime('%m') . $annee)
                        ->setIdFraisForfait('ETP')
                        ->setQuantite($etape);
            } else {
                $etp->setQuantite($etape + $etp->getQuantite());
            }
            $em->persist($etp);
        }
        if ($kilo) {
            $km = $repositorylff->findOneBy(
                    array('idVisiteur' => $session->get('id'), 'mois' => strftime('%m') . $annee, 'idFraisForfait' => 'KM')
            );
            if (!$km) {
                $km = new lignefraisforfait();
                $km->setIdVisiteur($session->get('id'))
                        ->setMois(strftime('%m') . $annee)
                        ->setIdFraisForfait('KM')
                        ->setQuantite($kilo);
                $em->persist($km);
            } else {
                $km->setQuantite($kilo + $km->getQuantite());
            }
        }
        if ($nuit) {
            $nt = $repositorylff->findOneBy(
                    array('idVisiteur' => $session->get('id'), 'mois' => strftime('%m') . $annee, 'idFraisForfait' => 'NUI')
            );
            if (!$nt) {
                $nt = new lignefraisforfait();
                $nt->setIdVisiteur($session->get('id'))
                        ->setMois(strftime('%m') . $annee)
                        ->setIdFraisForfait('NUI')
                        ->setQuantite($nuit);
            } else {
                $nt->setQuantite($nuit + $nt->getQuantite());
            }
            $em->persist($nt);
        }
        if ($resto) {
            $rt = $repositorylff->findOneBy(
                    array('idVisiteur' => $session->get('id'), 'mois' => strftime('%m') . $annee, 'idFraisForfait' => 'REP')
            );
            if (!$rt) {
                $rt = new lignefraisforfait();
                $rt->setIdVisiteur($session->get('id'))
                        ->setMois(strftime('%m') . $annee)
                        ->setIdFraisForfait('REP')
                        ->setQuantite($resto);
            } else {
                $rt->setQuantite($resto + $rt->getQuantite());
            }
            $em->persist($rt);
        }

        $em->flush();

        return $this->render('SaisieFicheFrais.html.twig', array('titre' => $titre, 'visiteur' => $visiteur));
    }

    public function horsforfaitAction(SessionInterface $session) {
        $request = Request::createFromGlobals();
        setlocale(LC_TIME, 'fr', 'fr_FR', 'fr_FR.ISO8859-1');
        $annee = strftime('%Y');
        $mois = strftime('%B');
        $titre = "Renseigner ma fiche de frais du mois de " . $mois . " " . $annee;
        $em = $this->getDoctrine()->getEntityManager();
        $visiteur = $em->getRepository('GSBFraisConnexionBundle:Visiteur')->findOneBy(array('id' => $session->get('id')));
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        $repositoryff = $this->getDoctrine()->getRepository(fichefrais::class);
        $fiche = $repositoryff->findOneBy(
                array('idVisiteur' => $session->get('id'), 'mois' => strftime('%m') . $annee)
        );

        if (!$fiche) {
            $fiche = new fichefrais();
            $fiche->setIdVisiteur($session->get('id'))
                    ->setMois(strftime('%m') . $annee)
                    ->setMontantValide(0)
                    ->setNbjustificatifs(0)
                    ->setDateModif(new \DateTime("now"))
                    ->setIdEtat('CR');
        }


        $date = $request->request->get('date');
        $libelle = $request->request->get('libelle');
        $montant = $request->request->get('montant');

        $ligneHorsForfait = new lignefraishorsforfait();
        $ligneHorsForfait->setDate(new \DateTime($date))
                ->setMois(strftime('%m') . $annee)
                ->setIdVisiteur($session->get('id'))
                ->setLibelle($libelle)
                ->setMontant($montant);

        $em->persist($ligneHorsForfait);
        $em->flush();

        return $this->render('SaisieFicheFrais.html.twig', array('titre' => $titre, 'visiteur' => $visiteur));
    }

    public function justificatifAction(SessionInterface $session) {
        $request = Request::createFromGlobals();
        setlocale(LC_TIME, 'fr', 'fr_FR', 'fr_FR.ISO8859-1');
        $annee = strftime('%Y');
        $mois = strftime('%B');
        $titre = "Renseigner ma fiche de frais du mois de " . $mois . " " . $annee;
        $em = $this->getDoctrine()->getEntityManager();
        $visiteur = $em->getRepository('GSBFraisConnexionBundle:Visiteur')->findOneBy(array('id' => $session->get('id')));
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        $justificatif = $request->request->get('justificatif');

        $repositoryff = $this->getDoctrine()->getRepository(fichefrais::class);
        $fiche = $repositoryff->findOneBy(
                array('idVisiteur' => $session->get('id'), 'mois' => strftime('%m') . $annee)
        );

        $fiche->setNbjustificatifs($fiche->getNbjustificatifs() + $justificatif)
                ->setDateModif(new \DateTime("now"));
        $em->persist($fiche);
        $em->flush();

        return $this->render('SaisieFicheFrais.html.twig', array('titre' => $titre, 'visiteur' => $visiteur));
    }

    public function dateLettre($date) {
        $tabmois = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];
        $m = (int) substr($date, 0, 2);
        $mois = $tabmois[$m - 1];
        $annee = substr($date, 2);
        $dateLettre = $mois . " " . $annee;

        return $dateLettre;
    }

    public function fichemoisAction(SessionInterface $session) {
        $request = Request::createFromGlobals();
        $em = $this->getDoctrine()->getEntityManager();
        $repositorylff = $this->getDoctrine()->getRepository(lignefraisforfait::class);
        $repositorylfhf = $this->getDoctrine()->getRepository(lignefraishorsforfait::class);
        $repositoryff = $this->getDoctrine()->getRepository(fichefrais::class);


        $visiteur = $em->getRepository('GSBFraisConnexionBundle:Visiteur')->findOneBy(array('id' => $session->get('id')));

        
        $query = $repositoryff->createQueryBuilder('f')
                ->select('f.mois')
                ->where('f.idVisiteur = :idVisiteur')
                ->setParameter('idVisiteur', $session->get('id'))
                ->getQuery();

        $lignes = $query->getResult();
        $vli = [];
        foreach ($lignes as $ligne) {
            $dateLettre = $this->dateLettre($ligne['mois']);
            $vli[$ligne['mois']] = $dateLettre;
        }
        $mois = $request->request->get('mois');

        $query = $repositorylff->createQueryBuilder('f')
                ->select('f.idFraisForfait', 'f.quantite')
                ->where('f.idVisiteur = :idVisiteur')
                ->andWhere('f.mois = :mois')
                ->setParameter('idVisiteur', $session->get('id'))
                ->setParameter('mois', $mois)
                ->getQuery();
        $result = $query->getScalarResult();
        $ligneforfait = array_column($result, 'quantite', 'idFraisForfait');
        $lignehorsforfait = $repositorylfhf->findBy(array('idVisiteur' => $session->get('id'), 'mois' => $mois));
        $fichefrais = $repositoryff->findOneBy(array('idVisiteur' => $session->get('id'), 'mois' => $mois));
        $mois = $this->dateLettre($mois);
        $em->flush();
        $nbJustificatifs = $fichefrais->getNbjustificatifs();
        return $this->render('FicheFraisVisiteur.html.twig', array('visiteur' => $visiteur, 'lignes' => $vli, 'ligneForfait' => $ligneforfait, 'ligneHorsForfait' => $lignehorsforfait, 'nbJustificatifs' => $nbJustificatifs, 'dateLettre' => $mois));
    }

    public function suppHorsForfaitAction($id, SessionInterface $session) {
        $em = $this->getDoctrine()->getEntityManager();
        $visiteur = $em->getRepository('GSBFraisConnexionBundle:Visiteur')->findOneBy(array('id' => $session->get('id')));
        
         $repositorylfhf = $this->getDoctrine()->getRepository(lignefraishorsforfait::class);
        $ligne = $repositorylfhf->find($id);
        $em->remove($ligne);
        $em->flush();

        return $this->render('Visiteur.html.twig', array('visiteur' => $visiteur));
    }

}
