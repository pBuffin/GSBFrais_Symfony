<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace GSBFrais\ConnexionBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use GSBFrais\ConnexionBundle\Entity\etat;

class LoadEtatData implements FixtureInterface {

    public function load(ObjectManager $manager) {

        $EtatArray = [['CL', 'Saisie clôturée'],
        ['CR', 'Fiche créée, saisie en cours' ],
        ['RB', 'Remboursée' ],
        ['VA', 'Validée et mise en paiement' ]
        ];

        foreach ($EtatArray as $key => $value) {

            $Etat = new etat();
            $Etat->setSigle($value[0]);
            $Etat->setLibelle($value[1]);
            
            $manager->persist($Etat);
            $manager->flush();
        }
    }

}
