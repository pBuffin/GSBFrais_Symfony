<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace GSBFrais\ConnexionBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use GSBFrais\ConnexionBundle\Entity\Comptable;

class LoadComptableData implements FixtureInterface {

    public function load(ObjectManager $manager) {

        $ComptableArray = [['bedos', 'christian', 'cbedos', 'secret'],
            ['auchon', 'paul', 'pauchon', 'pauchon']
        ];

        foreach ($ComptableArray as $key => $value) {

            $Comptable = new Comptable();
            $Comptable->setNom($value[0]);
            $Comptable->setPrenom($value[1]);
            $Comptable->setLogin($value[2]);
            $Comptable->setMdp($value[3]);

            $manager->persist($Comptable);
            $manager->flush();
        }
    }

}
