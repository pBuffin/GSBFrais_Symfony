<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace GSBFrais\ConnexionBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use GSBFrais\ConnexionBundle\Entity\fraisforfait;

class LoadfraisforfaitData implements FixtureInterface {

    public function load(ObjectManager $manager) {

        $fraisforfaitArray = [['ETP', 'Forfait Etape', '110.00'],
        ['KM', 'Frais Kilométrique', '0.62'],
        ['NUI', 'Nuitée Hôtel', '80.00'],
        ['REP', 'Repas Restaurant', '25.00']
        ];

        foreach ($fraisforfaitArray as $key => $value) {

            $fraisforfait = new fraisforfait();
            $fraisforfait->setSigle($value[0]);
            $fraisforfait->setLibelle($value[1]);
            $fraisforfait->setMontant($value[2]);

            $manager->persist($fraisforfait);
            $manager->flush();
        }
    }

}
